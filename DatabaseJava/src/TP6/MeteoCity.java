package TP6;

import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
public class MeteoCity {
	

		
	
	protected double lontitude;
	protected double latitude;
	protected int weatherId;
	protected String weathermain;
	protected String weatherdescrip;
	protected String  weathericon;
	protected String  weatherbase;
	protected double mainTemp;
	protected int pressure;
	protected int humidity;
	protected double temp_min;
	protected double temp_max;
	protected String visibility;
	protected double windSpeed;
	protected int cloud;
	protected String dtCloud;
	protected int sysType;
	protected int sysId;
	protected String sysName;
	protected int sysSunrise;
	protected int sysSunset;
	protected String sysTimeZone;
	protected String sysTimezoneID;
	protected String sysCityname;
	protected String sysCod;
	protected Date DateConsulter;
	protected SimpleDateFormat formater;

	public void setLontitude(double lont) {lontitude=lont;}
	public void setLatitude(double lati) {latitude=lati;}
	public void setWeatherId(int weathid) {weatherId=weathid;}
	public void setWeathermain(String weatmain) {weathermain=weatmain;}
	public void setWeatherdescrip(String weatdesp) {weatherdescrip=weatdesp;}
	public String getWeatherdescrip() {return weatherdescrip;}
	public void setWeathericon(String weaticon) {weathericon=weaticon;}
	public void setWeatherbase(String weatbase) {weatherbase=weatbase;}
	public void setmainTemp(double maintemp) {mainTemp=maintemp;}
	public double getmainTemp() {return mainTemp;}
	public void setpressure(int press) {pressure=press;}
	public int getpressure() {return pressure;}
	public void sethumidity(int humi) {humidity=humi;}
	public int gethumidity() {return humidity;}
	public void settempMin(double tempMin) {temp_min=tempMin;}
	public void settempMax(double tempMan) {temp_max=tempMan;}
	public void setvisibility(String visi) {visibility=visi;}
	public void setwindSpeed(double speed) {windSpeed=speed;}
	public double getwindSpeed() {return windSpeed;}
	public void setcloud(int clo) {cloud=clo;}
	public void setdtCloud(String clodt) {dtCloud=clodt;}
	public void setsysType(int systype) {sysType=systype;}
	public void setsysID(int sysid) {sysId=sysid;}
	public void setsysName(String sysname) {sysName=sysname;}
	public void setsysSunrise(int sunraise) {sysSunrise=sunraise;}
	public void setsysSunset(int sunset) {sysSunset=sunset;}
	public void setsysTmeZone(String timezone) {sysTimeZone=timezone;}
	public void setsysTmeZoneID(String timezoneid) {sysTimezoneID=timezoneid;}
	public void setsysZoneName(String zonename) {sysCityname=zonename;}
	public String getsysZoneName( ) {return sysCityname;}
	public void setsysZoneCod(String syscod) {sysCod=syscod;}
	public void setdateConsulter() {
		DateConsulter = new Date();
	   	formater = new SimpleDateFormat("'consulté au ' dd/MM/yyyy 'à' hh:mm:ss");
		
		}
	public String toString(){
		StringBuilder sb = new StringBuilder("date consulté :");
		sb.append(formater.format(DateConsulter));
		sb.append(" city: : ");
		sb.append(sysCityname);   //String
		sb.append("  pressure: "); 
		sb.append(pressure);	//int
		sb.append("  humidity: ");
		sb.append(humidity);    //int
		sb.append("  weatherdescrip : ");
		sb.append(weatherdescrip);   //String
		sb.append("  maintemp : ");
		sb.append(mainTemp);   //double
		return sb.toString();
	}
	public   void afficher(){
		
		System.out.println(formater.format(DateConsulter));
		System.out.println("city:            " +sysCityname);
		System.out.println("c:        " +pressure);
		System.out.println("humidity: 		 " +humidity);
		System.out.println("weatherdescrip : " +weatherdescrip);
		System.out.println("sysSunrise:      " +sysSunrise);
		System.out.println("sysSunset:       " +sysSunset);
		System.out.println("lontitude:       " +lontitude);
		System.out.println("latitude:        " +latitude);
		System.out.println("visibility:      " +visibility);
		System.out.println("mainTemp:       " + mainTemp);
		
	 }

/*String nameCity, int cityPressure, double humidity, String weatherdescrip, double WindSpeed */

	 


	
	
	public static void CreationTable(Connection conn) throws SQLException {
	
		  Statement st = conn.createStatement();
	        StringBuilder sb = new StringBuilder();
	        sb.append("CREATE TABLE IF NOT EXISTS weatherCity (");
	        sb.append("id  INTEGER PRIMARY KEY NOT NULL,");
	        sb.append("city VARCHAR(100) NOT NULL,");
			sb.append("pressure INTEGER NOT NULL,");
			sb.append("humidity FLOAT DEFAULT null,");
			sb.append("weatherdescrip VARCHAR(100) NOT NULL,");
			sb.append("maintemp DOUBLE DEFAULT null");
			sb.append(")");

	        st.executeUpdate(sb.toString());
	        st.close();
	    }

	
	 public static void insert(Connection conn,int id,String nameCity, int cityPressure, int humidity, String weatherdescrip, double maintemp ) throws SQLException {
	        
		 		PreparedStatement ps = null;
		 		String INSERT_SQL = "INSERT INTO weatherCity(id,city, pressure, humidity, weatherdescrip, maintemp) VALUES(?, ?, ?, ?, ?, ?)";
	            ps = conn.prepareStatement(INSERT_SQL);
	            ps.setInt(1, id);
	            ps.setString(2, nameCity);
	            ps.setInt(3, cityPressure);
	            ps.setInt(4, humidity);
	            ps.setString(5, weatherdescrip);
	            ps.setDouble(6, maintemp);  
	         
	            ps.executeUpdate();

	     
	    }


	 
	 
	 public static void selectAll(Connection conn){
	        String sql = "SELECT id,city, pressure, humidity,weatherdescrip,maintemp FROM weatherCity";
	        
	        try (
	             Statement stmt  = conn.createStatement();
	             ResultSet rs    = stmt.executeQuery(sql)){
	            
	            while (rs.next()) {
	                System.out.println(rs.getInt("id") +  "\t" + 
	                				   rs.getString("city") +  "\t" + 
	                                   rs.getInt("pressure") + "\t" +
	                                   rs.getInt("humidity") + "\t" +
	                                   rs.getString("weatherdescrip") + "\t" +
	                                   rs.getDouble("maintemp"));
	            }
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	    }
	 public static void trierparNomCity(Connection conn){
	        String sql = "SELECT id,city, pressure, humidity,weatherdescrip,maintemp FROM weatherCity";
	        
	        try (
	             Statement stmt  = conn.createStatement();
	             ResultSet rs    = stmt.executeQuery(sql)){
	            
	            while (rs.next()) {
	                System.out.println(rs.getInt("id") +  "\t" + 
	                					rs.getString("city") +  "\t" + 
	                                   rs.getInt("pressure") + "\t" +
	                                   rs.getInt("humidity") + "\t" +
	                                   rs.getString("weatherdescrip") + "\t" +
	                                   rs.getDouble("maintemp"));
	            }
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	    }
	 public static void tablesupprimer(Connection conn) throws SQLException {
	        Statement stmt = conn.createStatement();
	        stmt.executeUpdate("DROP TABLE weatherCity");
	        stmt.close();
	    }
	 public static void tableSupLine(Connection conn,int a) throws SQLException {
	        Statement stmt = conn.createStatement();
	        StringBuilder sb = new StringBuilder();
	        sb.append("DELETE FROM weatherCity ");
	        sb.append("WHERE  ID =" );
	        sb.append(a);
	        stmt.executeUpdate(sb.toString());
	        stmt.close();
	    }
	 public static void videTable(Connection conn) throws SQLException {
	        Statement stmt = conn.createStatement();
	        StringBuilder sb = new StringBuilder();
	        sb.append("DELETE FROM weatherCity ");
	     
	        stmt.executeUpdate(sb.toString());
	        stmt.close();
	    }
	 public static void trierParNom(Connection conn,String nom) throws SQLException {
	
		 PreparedStatement ps = null;
	 		//String SELECT_SQL = "SELECT * FROM weatherCity WHERE city = (city) VALUES( ?)";
	 		String query = "SELECT id,city, pressure, humidity,weatherdescrip,maintemp FROM weatherCity  WHERE UPPER(city) LIKE ?";

	 		ps = conn.prepareStatement(query);
            ps.setString(1, nom);
            ResultSet rs    = ps.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getInt("id") +  "\t" + 
                				   rs.getString("city") +  "\t" + 
                                   rs.getInt("pressure") + "\t" +
                                   rs.getInt("humidity") + "\t" +
                                   rs.getString("weatherdescrip") + "\t" +
                                   rs.getDouble("maintemp"));
            }
	    }
	 public static void update(Connection conn,int id,String nameCity,int pressure,int humidity, String weatherdescrip, double maintemp) throws SQLException {
	        String sql = "UPDATE weatherCity SET city = ? , "
	                + "pressure = ? ,"
	                + "humidity = ? ,"
	                + "weatherdescrip = ?, "
	                + "maintemp = ? "
	                + "WHERE id = ?";
	        PreparedStatement ps = null;
            ps = conn.prepareStatement(sql);
            ps.setString(1, nameCity);
            ps.setInt(2, pressure);
            ps.setInt(3, humidity);
            ps.setString(4, weatherdescrip);
            ps.setDouble(5, maintemp);  
            ps.setInt(6, id);
	        ps.executeUpdate();
	       
	    }
	 public static void updateName(Connection conn,int id,String nameCity) throws SQLException {
	        String sql = "UPDATE weatherCity SET city = ?  "
	                + "WHERE id = ?";
	        PreparedStatement ps = null;
         ps = conn.prepareStatement(sql);
         ps.setString(1, nameCity);
         ps.setInt(2, id);
	        ps.executeUpdate();
	       
	    }
	 public static void updatePressure(Connection conn,int id, int pressure) throws SQLException {
	        String sql = "UPDATE weatherCity SET pressure = ?  "
	                + "WHERE id = ?";
	        PreparedStatement ps = null;
         ps = conn.prepareStatement(sql);
         ps.setInt(1, pressure);
         ps.setInt(2, id);
	        ps.executeUpdate();
	       
	    }
	 public static void updateHumidity(Connection conn,int id,int humidity) throws SQLException {
	        String sql = "UPDATE weatherCity SET humidity = ? "
	                + "WHERE id = ?";
	        PreparedStatement ps = null;
         ps = conn.prepareStatement(sql);
         ps.setInt(1, humidity);
         ps.setInt(2, id);
	        ps.executeUpdate();
	       
	    }
	 public static void updateWeatherdescrip(Connection conn,int id,String weatherdescrip) throws SQLException {
	        String sql = "UPDATE weatherCity SET weatherdescrip = ? "
	                + "WHERE id = ?";
	        PreparedStatement ps = null;
      ps = conn.prepareStatement(sql);
      ps.setString(1, weatherdescrip);
      ps.setInt(2, id);
	        ps.executeUpdate();
	       
	    }
	 public static void updateMaintemp(Connection conn,int id,double  maintemp) throws SQLException {
	        String sql = "UPDATE weatherCity SET maintemp = ? "
	                + "WHERE id = ?";
	        PreparedStatement ps = null;
   ps = conn.prepareStatement(sql);
   ps.setDouble(1, maintemp);
   ps.setInt(2, id);
	        ps.executeUpdate();
	       
	    }
	}
	
	
	

