package TP6;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class Network<meteoCitys>{
	public Network(List<MeteoCity>meteoCitys) {
		HttpURLConnection urlConnection =null;
		try {
		URL url = new URL("https://api.openweathermap.org/data/2.5/weather?q=clermont-ferrand&appid=c62ff9852f25d0252f6681f2d0205609");
		urlConnection = (HttpURLConnection) url.openConnection();
		InputStream  in = new  BufferedInputStream(urlConnection.getInputStream ());
		JsonParser j=new JsonParser();
		
		j.readJson(in,meteoCitys);
		
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
		
			if(urlConnection !=null) {
				urlConnection.disconnect();
			}
			
		}
	}

	public Network(String namecity,List<MeteoCity>meteoCitys) {
		HttpURLConnection urlConnection =null;
		try {
	    URL myURL = new URL("https://api.openweathermap.org/data/2.5/weather?q="+namecity+"&appid=c62ff9852f25d0252f6681f2d0205609");
			
		urlConnection = (HttpURLConnection) myURL.openConnection();
		InputStream  in = new  BufferedInputStream(urlConnection.getInputStream ());
		JsonParser j=new JsonParser();
		j.readJson(in,meteoCitys);
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("la ville "+namecity+" ne exister pas : "+e);
		} finally {
		
			if(urlConnection !=null) {
				urlConnection.disconnect();
			}
		}
	}
}
