package TP6;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Phaser;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import com.google.gson.*;
import java.util.ArrayList;




import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {
	
	public static List<MeteoCity> meteoCitys;

	public   String afficher(List<MeteoCity> meteoCitys){
		StringBuilder string = new StringBuilder();
		for (MeteoCity meteoCity : meteoCitys) {
			string.append(meteoCity.toString());
			string.append(System.lineSeparator());
			}
		return string.toString();
	 }
    public void menu() throws ClassNotFoundException, SQLException {
    	meteoCitys = new ArrayList<MeteoCity>();
    	System.out.println("***********************************************");  
    	Class.forName("org.sqlite.JDBC");
		Connection conn = DriverManager.getConnection("jdbc:sqlite:MeteoCity.db");
		System.out.println("*  Bien connecter avec MeteoCity.db   	      *");  
		 
		MeteoCity.CreationTable(conn);  
		MeteoCity.videTable(conn); 
		System.out.println("*  Bien creer/connecter la table  weatherCity *");
	//	MeteoCity.tablesupprimer(conn);
    	int flag=0;
		while(flag==0){

			Scanner myObj = new Scanner(System.in);
			System.out.println("***********************************************");  
			System.out.println(" 1. connecter (Clermont-Ferrand) par defaut et stocker dans la table weatherCity");            
			System.out.println(" 2. connecter avec le city vous choissiez et stocker dans la table weatherCity  ");      
			System.out.println(" 3. Afficher la table weatherCity "); 
			System.out.println(" 4. trier par nom de city "); 
			System.out.println(" 5. supprimer par ID "); 
			System.out.println(" 6. update par ID ");  
			System.out.println(" 7. supprimer la table ");  
			System.out.println(" 9. quit ");  
			System.out.println("***********************************************");     
			String choisir = myObj.nextLine();

			switch(choisir) {
			
				  case "1":
					 Network network=new Network(meteoCitys);
					int a =meteoCitys.size();
					String name =meteoCitys.get(a-1).getsysZoneName();
					int pressure=meteoCitys.get(a-1).getpressure();
					int humi=meteoCitys.get(a-1).gethumidity();
					String descri=meteoCitys.get(a-1).getWeatherdescrip();
					double maintemp =meteoCitys.get(a-1).getmainTemp();
				//	MeteoCity.supprimerTable(conn);
					MeteoCity.insert(conn,a,name, pressure, humi,descri,maintemp);
				break; 
				case "2":
					  System.out.println("***************");  
					  System.out.println(" Nom de ville ?");       
					  String nomVille = myObj.nextLine(); 
					  Network network1=new Network(nomVille,meteoCitys);
					   	a =meteoCitys.size();
						 name =meteoCitys.get(a-1).getsysZoneName();
						 pressure=meteoCitys.get(a-1).getpressure();
						 humi=meteoCitys.get(a-1).gethumidity();
						 descri=meteoCitys.get(a-1).getWeatherdescrip();
						 maintemp =meteoCitys.get(a-1).getmainTemp();
						MeteoCity.insert(conn,a,name, pressure, humi,descri,maintemp);
				break; 
				case "3":
					// System.out.println(afficher(meteoCitys));  
					  MeteoCity.selectAll(conn);
				break; 
				case "4":
					 System.out.println("***************");  
					  System.out.println(" Nom de ville select from table ?");       
					  String nomcity = myObj.nextLine(); 
					  MeteoCity.trierParNom(conn,nomcity);
				break; 
				case "5":
					 System.out.println("***************");  
					  System.out.println(" supprimer ID ? ?");       
					  String nomid = myObj.nextLine(); 
					  MeteoCity.tableSupLine(conn,Integer.valueOf(nomid));
				break; 
				case "6":
					 System.out.println("***************");  
					  System.out.println(" update ID ? ?");       
					  String update = myObj.nextLine(); 
					  System.out.println("***************");  
					  System.out.println("1. update nom de city");   
					  System.out.println("2. update nom de pressure"); 
					  System.out.println("3. update nom de humidity"); 
					  System.out.println("4. update nom de weatherdescrip"); 
					  System.out.println("5. update nom de maintemp"); 
					  String cho = myObj.nextLine();
					  switch(cho) {
					  
						  case "1":
							  System.out.println(" update nom city ? ?");   
							  String cityUpdate = myObj.nextLine(); 
							  MeteoCity.updateName(conn,Integer.valueOf(update),cityUpdate);
							break; 
						  case "2":
							  System.out.println(" update pressure city ? ?");   
							  String Updatepressure = myObj.nextLine(); 
							  MeteoCity.updatePressure(conn,Integer.valueOf(update),Integer.valueOf(Updatepressure));
								break; 
						  case "3":
							  System.out.println(" update humidity city ? ?");   
							  String Updatehumidity = myObj.nextLine(); 
							  MeteoCity.updateHumidity(conn,Integer.valueOf(update),Integer.valueOf(Updatehumidity));
								break; 
						  case "4":
							  System.out.println(" update weatherdescrip city ? ?");   
							  String Updateweatherdescrip = myObj.nextLine(); 
							  MeteoCity.updateWeatherdescrip(conn,Integer.valueOf(update),Updateweatherdescrip);
								break; 
						  case "5":
							  System.out.println(" update maintemp city ? ?");   
							  String Updatemaintemp = myObj.nextLine(); 
							  MeteoCity.updateMaintemp(conn,Integer.valueOf(update),Double.valueOf(Updatemaintemp));
								break; 
						  default :
								 System.out.println("update par défault");
								 MeteoCity.update(conn,Integer.valueOf(update),"Lille",992,74,"few clouds",279.25);
						    break;
					  }
					  
					  
					 
				break; 
				case "7":
					MeteoCity.tablesupprimer(conn);
					break; 
				case "9":
					System.exit(1);
					break; 
				 default :
					 System.out.println("Aucune option");
				break;
			
			}
		
			}
		}

	public static void main (String[] args) throws Exception, SQLException  {
		Main main =new Main();
		main.menu();
		
	}	
}

