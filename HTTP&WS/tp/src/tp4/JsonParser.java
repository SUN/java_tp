package tp4;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.Date;
public class JsonParser  {
	public void readJson(InputStream input,List<MeteoCity>meteoCitys) {
		try{
			InputStreamReader  readContenu=new  InputStreamReader(input);
			BufferedReader  in = new  BufferedReader(readContenu);
			String  texte = in.readLine ();
			jsonPasrer(texte,meteoCitys);
			//System.out.println(texte);
		} catch( Exception e){
			System.out.println("error");
			e.printStackTrace ();
		}
	}
	public void jsonPasrer(String text,List<MeteoCity>meteoCitys) throws JSONException {
		
	
	   
	 	JSONObject jsonObject = new JSONObject(text);
	 	JSONObject coordObject = jsonObject.getJSONObject("coord");
	    JSONArray weatherArray = jsonObject.getJSONArray("weather");
	    JSONObject weatherObject = (JSONObject)weatherArray.get(0);
	    String baseObject = jsonObject.get("base").toString();
	    JSONObject mainObject = jsonObject.getJSONObject("main");
	    String visiObject = jsonObject.get("visibility").toString();
	    JSONObject windObject = jsonObject.getJSONObject("wind");
	    JSONObject cloudObject = jsonObject.getJSONObject("clouds");
	    String dtObject = jsonObject.get("dt").toString();
	    JSONObject sysObject = jsonObject.getJSONObject("sys");
	    String timezoneObject = jsonObject.get("timezone").toString();
	    String idObject =jsonObject.get("id").toString();
	    String nameObject = jsonObject.get("name").toString();
	    String codObject = jsonObject.get("cod").toString();
		MeteoCity metrocity=new MeteoCity();
	    metrocity.setLontitude((double)coordObject.get("lon"));
	    metrocity.setLatitude((double) coordObject.get("lat"));
	    metrocity.setWeatherId((int) weatherObject.get("id"));
	    metrocity.setWeathermain( (String)weatherObject.get("main"));
	    metrocity.setWeatherdescrip((String)weatherObject.get("description"));
	    metrocity.setWeathericon( (String)weatherObject.get("icon"));
	    metrocity.setWeatherbase(baseObject);
	    metrocity.setpressure((int)mainObject.get("pressure"));
	    metrocity.setmainTemp((double)mainObject.get("temp"));
	    metrocity.sethumidity((int)mainObject.get("humidity"));
	    metrocity.settempMin((double)mainObject.get("temp_min"));
	    metrocity.settempMax((double)mainObject.get("temp_max"));
	    metrocity.setvisibility(visiObject);
	    metrocity.setwindSpeed((double)windObject.get("speed"));
	    metrocity.setcloud((int)cloudObject.get("all")); 
	    metrocity.setdtCloud(dtObject);	   
	    metrocity.setsysType((int)sysObject.get("type"));
	    metrocity.setsysID((int)sysObject.get("id"));
	    metrocity.setsysName((String)sysObject.get("country"));
	    metrocity.setsysSunrise((int)sysObject.get("sunrise"));
		metrocity.setsysSunset((int) sysObject.get("sunset"));
		metrocity.setsysTmeZone(timezoneObject);
		metrocity.setsysTmeZoneID(idObject); 
		metrocity.setsysZoneName(nameObject);	 
		metrocity.setsysZoneCod(codObject);
		metrocity.setdateConsulter();
		meteoCitys.add(metrocity);
		metrocity.afficher();
	}
	
}
