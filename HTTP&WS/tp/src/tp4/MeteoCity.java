package tp4;

import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;
public class MeteoCity {
	protected double lontitude;
	protected double latitude;
	protected int weatherId;
	protected String weathermain;
	protected String weatherdescrip;
	protected String  weathericon;
	protected String  weatherbase;
	protected double mainTemp;
	protected int pressure;
	protected int humidity;
	protected double temp_min;
	protected double temp_max;
	protected String visibility;
	protected double windSpeed;
	protected int cloud;
	protected String dtCloud;
	protected int sysType;
	protected int sysId;
	protected String sysName;
	protected int sysSunrise;
	protected int sysSunset;
	protected String sysTimeZone;
	protected String sysTimezoneID;
	protected String sysCityname;
	protected String sysCod;
	protected Date DateConsulter;
	protected SimpleDateFormat formater;

	public void setLontitude(double lont) {lontitude=lont;}
	public void setLatitude(double lati) {latitude=lati;}
	public void setWeatherId(int weathid) {weatherId=weathid;}
	public void setWeathermain(String weatmain) {weathermain=weatmain;}
	public void setWeatherdescrip(String weatdesp) {weatherdescrip=weatdesp;}
	public void setWeathericon(String weaticon) {weathericon=weaticon;}
	public void setWeatherbase(String weatbase) {weatherbase=weatbase;}
	public void setmainTemp(double maintemp) {mainTemp=maintemp;}
	public void setpressure(int press) {pressure=press;}
	public void sethumidity(int humi) {humidity=humi;}
	public void settempMin(double tempMin) {temp_min=tempMin;}
	public void settempMax(double tempMan) {temp_max=tempMan;}
	public void setvisibility(String visi) {visibility=visi;}
	public void setwindSpeed(double speed) {windSpeed=speed;}
	public void setcloud(int clo) {cloud=clo;}
	public void setdtCloud(String clodt) {dtCloud=clodt;}
	public void setsysType(int systype) {sysType=systype;}
	public void setsysID(int sysid) {sysId=sysid;}
	public void setsysName(String sysname) {sysName=sysname;}
	public void setsysSunrise(int sunraise) {sysSunrise=sunraise;}
	public void setsysSunset(int sunset) {sysSunset=sunset;}
	public void setsysTmeZone(String timezone) {sysTimeZone=timezone;}
	public void setsysTmeZoneID(String timezoneid) {sysTimezoneID=timezoneid;}
	public void setsysZoneName(String zonename) {sysCityname=zonename;}
	public void setsysZoneCod(String syscod) {sysCod=syscod;}
	public void setdateConsulter() {
		DateConsulter = new Date();
	   	formater = new SimpleDateFormat("'consulté au ' dd/MM/yyyy 'à' hh:mm:ss");
		
		}
	public String toString(){
		StringBuilder sb = new StringBuilder("date consulté :");
		sb.append(formater.format(DateConsulter));
		sb.append(" city: : ");
		sb.append(sysCityname);
		sb.append("  pressure: ");
		sb.append(pressure);
		sb.append("  humidity: ");
		sb.append(humidity);
		sb.append("  weatherdescrip : ");
		sb.append(weatherdescrip);
		sb.append("  windSpeed : ");
		sb.append(windSpeed);
		return sb.toString();
	}
	public   void afficher(){
		
		System.out.println(formater.format(DateConsulter));
		System.out.println("city:            " +sysCityname);
		System.out.println("pressure:        " +pressure);
		System.out.println("humidity: 		 " +humidity);
		System.out.println("weatherdescrip : " +weatherdescrip);
		System.out.println("sysSunrise:      " +sysSunrise);
		System.out.println("sysSunset:       " +sysSunset);
		System.out.println("lontitude:       " +lontitude);
		System.out.println("latitude:        " +latitude);
		System.out.println("visibility:      " +visibility);
		System.out.println("windSpeed:       " +windSpeed);
		
	 }
	
	
}
