import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale; 

public class Book {
				protected String NomBook;
				protected String Prix;
				protected Date DateAjouter;
				protected SimpleDateFormat formater;
				protected String Categorie;
				protected double Note;
		
				Book() 
				{
		   			NomBook="";
		  		}

				Book(String nom) 
				{
		   			NomBook=nom;
					DateAjouter = new Date();
				   	formater = new SimpleDateFormat("'le' dd/MM/yyyy 'à' hh:mm:ss");
					System.out.println(formater.format(DateAjouter));
		  		}
		
				Book(String nom,String prix) 
				{
		   			NomBook=nom;
					Prix=prix;
					DateAjouter= new Date();
   				   	formater = new SimpleDateFormat("'le' dd/MM/yyyy 'à' hh:mm:ss");
 					System.out.println(formater.format(DateAjouter));
		  		}
				Book(String nom,String prix,String categ) 
				{
		   			NomBook=nom;
					Prix=prix;
					DateAjouter= new Date();
   				   	formater = new SimpleDateFormat("'le' dd/MM/yyyy 'à' hh:mm:ss");
 					System.out.println(formater.format(DateAjouter));
					Categorie=categ;
		  		}
			
				public String GetBook() 
				{
                    
					return NomBook;
				}
				public void SetBook(String nom) 
				{
					NomBook=nom;
				}
		
				public String toString(){
					StringBuilder sb = new StringBuilder("NomBook: ");
					sb.append(NomBook);
					sb.append("  Prix: ");
					sb.append(Prix);
					sb.append("  Categorie: ");
					sb.append(Categorie);
					sb.append("  Date Ajouté: ");
					sb.append(formater.format(DateAjouter));
					return sb.toString();
				}

		
				
}
