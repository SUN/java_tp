package TP7;
import java.lang.Thread;
import java.util.List;
import java.util.Random;
import java.util.Date;

public class ThreadGerer implements Runnable{
	private Thread thread;
	private static int threadNombre=1;
	private int threadid;
	
	private Gererlist list;
	private int etudiant_number;
	private long sleep_time;
	
	public ThreadGerer() {
		threadNombre = threadNombre;
		threadNombre++;
		thread = new Thread(this);
	}

	public ThreadGerer(Gererlist etudiantList, int E_number, long sleep_time){
		this();
	    this.list=etudiantList;
	    this.etudiant_number=E_number;
	    this.sleep_time=sleep_time;
		thread.start();
	}
	
	
	public Thread getThread() {
		return thread;
	}
		
	
	
	static int id=0;
	public static String getRandomString(int length){
	
	     String str="abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
	     Random random=new Random();
	     StringBuffer sb=new StringBuffer();
	     for(int i=0;i<length;i++){
	       int number=random.nextInt(43);
	       sb.append(str.charAt(number));
	     }
	     return sb.toString();
	 }
	
	public  Etudiant creerEtudiantAleatoire() {
		
		 Random rand = new Random(); 
		 Date d =new Date(-946771200000L + rand.nextLong() % (70L * 365 * 24 * 60 * 60 * 1000));
		 Etudiant etudiant = new Etudiant(0, null, null, null, null, null, null, null); 
		 String nom=getRandomString(2);
		 String prenom=getRandomString(5);
		 etudiant.setIdentifiant(id);
		 etudiant.setDateConsulter(d);
		 etudiant.setNom(nom);
		 etudiant.setPrenom(prenom);
		// System.out.println(etudiant.toString()); 
		 id++;
		 return etudiant;
	}	
	
	public  void addEtudiantlist(int i) {
		
	
		for(int a=0;a<i;a++) {
			Random rand = new Random(); 
			 Etudiant etudiant = new Etudiant(id, null, null, null, null, null, null, null); 
			 Date d =new Date(-946771200000L + rand.nextLong() % (70L * 365 * 24 * 60 * 60 * 1000));
			 String nom=getRandomString(5);
			 String prenom=getRandomString(5);
			 etudiant.setNom(nom);
			 etudiant.setLieu("Lieu");
			 etudiant.setNational("national");
			 etudiant.setParcours("parcours");
			 etudiant.setPrenom(prenom);
			 etudiant.setParcours("Parf");
			 etudiant.setDateConsulter(d);
			 System.out.println(etudiant.toString()); 
			 id++;
			
			 Gererlist f=new Gererlist();
			 f.add(etudiant);
		}
		
	
	}	
	
	
	public  Etudiant addEtudiantlist() {
		
	
			Random rand = new Random(); 
			 Etudiant etudiant = new Etudiant(id, null, null, null, null, null, null, null); 
			 Date d =new Date(-946771200000L + rand.nextLong() % (70L * 365 * 24 * 60 * 60 * 1000));
			// String nom=getRandomString(5);
			// String prenom=getRandomString(5);
			 String nomEtudiant =Enums.random(nomEtudiant.class).toString();
			 String prenomEtudiant =Enums.random(prenomEtudiant.class).toString();
			 String lieuEtudiant =Enums.random(lieuEtudiant.class).toString();
			 String nationEtudiant =Enums.random(nationalEtudiant.class).toString();
			 String parcourEtudiant =Enums.random(parcourEtudiant.class).toString();
			 etudiant.setNom(nomEtudiant);
			 etudiant.setLieu(lieuEtudiant);
			 etudiant.setNational(nationEtudiant);
			 etudiant.setParcours(parcourEtudiant);
			 etudiant.setPrenom(prenomEtudiant);
			 etudiant.setDateConsulter(d);
			 System.out.println(etudiant.toString()); 
			 id++;
		
			 return etudiant;
		}

	 public void addtoList() {
		   for (int i = 0; i < etudiant_number; i++) {
				Etudiant student = addEtudiantlist();
				try {
					Thread.sleep(sleep_time);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				list.add(student, sleep_time);
			}

	   }
	   public static class Enums {
	        private static int random=(int)(Math.random()*10);// 生成种子
	        private static  Random rand = new Random(random);
	    
	        public static <T extends Enum<T>> T random(Class<T> ec) {
	            return random(ec.getEnumConstants());
	        }
	    
	        public static  <T> T random(T[] values) {
	            return values[rand.nextInt(values.length)];
	        }
	    }

	   
		
		
		enum nomEtudiant {
			Marie  , Nathalie, Isabelle, Catherine, Monique, Sylvie, Françoise,Jean,Michel,Dominique,Christophe,Pierre
		}
		enum prenomEtudiant {
			Martin  , Bernard, Dubois, Thomas, Robert, Richard, Petit,Durand,Leroy,Moreau,Tessier,Cesbron
		}
		enum lieuEtudiant {
			Paris  , Marseille, Lyon, Toulouse, Nice, Nantes, Strasbourg,Montpellier,Bordeaux,Lille,Rennes,ClermontFerrand
		}
		enum nationalEtudiant {
			Venezuela  , UnitedStates, UnitedKingdom, UnitedEmirates, China, Thailand, Tanzania,Sweden,Singapore,Russia,Portugal,Poland,Norway,France
		}
		enum parcourEtudiant {
			C  , java, baseDonnee, UML, Elertronique, Physique, Chimie ,Anglais, Reseau,Security
		}
		    public  void Test() {
		        for (int i = 0; i < 7; i++){
		        	String nom =Enums.random(nomEtudiant.class).toString();
		            System.out.println("i="+i+"---"+nom + " ");
		            }
		    }
	


	public void run() {
	      System.out.println("Running " +  threadid );
	      try {
	    	  addtoList();
	    	  Thread.sleep (sleep_time);
	         }
	      
		catch (InterruptedException e) {
	         System.out.println("Thread " +  threadid + " interrupted.");
	      }
	     
	   }
	

		
	
}
