package TP7;

import java.util.ArrayList;
import java.util.List;

/*
 * 5min    1. Greer la clasee etudiant  ✅
 * 16min   2. Générer une list de etudiant  ✅
 * 25min   3. Creer plusieurs thread qui ajouter des etudiant à une meme!!!!!!! list ✅
 * 5min    4. Mesurer le temp d'excution +muliti run +moyenne ✅
 * 15min   5. le reste    ✅ calculer le temp gagne
 * 15min   5. écrire dans le disque   ✅ calculer le temp gagne
 * */
public class Main{

   public static void gestionThread(int nombreThread, int etudiant_nombre, long sleep) throws Exception {
	   long start = System.currentTimeMillis();
	   Gererlist list = new Gererlist();
	   List<ThreadGerer> listThread = new ArrayList<ThreadGerer>();
	   for(int i=0;i<nombreThread;i++) {
		   listThread.add(new ThreadGerer(list, etudiant_nombre, sleep));
		  
	   }
	   for (int i = 0; i< nombreThread; i++) {
		listThread.get(i).getThread().join();
	   }
	   
	   EcrireDisque.creerfiche("ficherEtudiant", list);
		long finish = System.currentTimeMillis();
		System.out.println("nombre Thread : "+nombreThread);
		System.out.println("nombre Etudiant : " + nombreThread*etudiant_nombre);
		System.out.println("temp de sleep : "+ sleep);
		System.out.println("temp utilise : : " + (finish - start));
   }

   public static void gestionThreadEcrire(int nombreThread, int etudiant_nombre, long sleep,String nomFicher) throws Exception {
	   long start = System.currentTimeMillis();
	   Gererlist list = new Gererlist();
	   List<ThreadGerer> listThread = new ArrayList<ThreadGerer>();
	   for(int i=0;i<nombreThread;i++) {
		   listThread.add(new ThreadGerer(list, etudiant_nombre, sleep));
		  
	   }
	   for (int i = 0; i< nombreThread; i++) {
		listThread.get(i).getThread().join();
	   }
	   
	   EcrireDisque.creerfiche(nomFicher, list);
		long finish = System.currentTimeMillis();
		System.out.println("nombre Thread : "+nombreThread);
		System.out.println("nombre Etudiant : " + nombreThread*etudiant_nombre);
		System.out.println("temp de sleep : "+ sleep);
		System.out.println("temp utilise : : " + (finish - start));
   }
	public static void main(String[] argv) throws Exception {
	/*	Gererlist list1 = new Gererlist();
		//ThreadGerer f=new ThreadGerer(list1,158,10);
		 Runnable r = new ThreadGerer (list1,158,10);
		   Thread monThread = new Thread (r);
		   monThread.start ();
	*/
		//gestionThread(1,150,10);
		//gestionThread(3,50000,10);  // nombre de Thread   - nom de étudiants sleep temp
		gestionThreadEcrire(10,15000,0,"Thread5");
		
	}


}



