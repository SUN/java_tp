package TP7;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.text.SimpleDateFormat;
public class Etudiant {
	protected int identifiant;
	protected String national;
	protected String nom;
	protected String prenom;
	protected String lieu;
	protected String parcours;
	protected Date Datenessance;
	protected SimpleDateFormat formater;
	
	public Etudiant() {
		
	}

	public Etudiant(int identifiant, String national, String nom, String prenom, String lieu, String parcours,
			Date date, SimpleDateFormat formater) {
		super();
		this.identifiant = identifiant;
		this.national = national;
		this.nom = nom;
		this.prenom = prenom;
		this.lieu = lieu;
		this.parcours = parcours;
		Datenessance = date;
		this.formater = formater;
	}
	@Override
	public String toString() {
		return "Etudiant [identifiant=" + identifiant + ", national=" + national + ", nom=" + nom + ", prenom=" + prenom
				+ ", lieu=" + lieu + ", parcours=" + parcours + ", DateConsulter=" + Datenessance + "]";
	}

	public int getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(int identifiant) {
		this.identifiant = identifiant;
	}

	public String getNational() {
		return national;
	}

	public void setNational(String national) {
		this.national = national;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public String getParcours() {
		return parcours;
	}

	public void setParcours(String parcours) {
		this.parcours = parcours;
	}

	public Date getDateConsulter() {
		return Datenessance;
	}


	public void setDateConsulter(Date dateConsulter) {
		Datenessance = dateConsulter;
	}

	public SimpleDateFormat getFormater() {
		return formater;
	}


	public void setFormater(SimpleDateFormat formater) {
		this.formater = formater;
	}
	
	
	
}
