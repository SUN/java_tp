package tp3;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class MetroTest  {

	 private List<Metro> metros;
	 
	@Before
 
	public void beforeTest() throws Exception {
		metros = new ArrayList<Metro>();
		LiredeFiche file= new LiredeFiche();
	    file.lire(metros);

	}
	@Test
	public void testlire() throws IOException {

	    assertEquals(1975, metros.get(0).idStation);
	    assertEquals("2.33871281165883", metros.get(0).Coordonnee);
	    assertEquals("48.8844176451841", metros.get(0).geographique);
	    assertEquals("Abbesses", metros.get(0).nomStation);
	    assertEquals("PARIS-18EME", metros.get(0).arrodisement);
	    assertEquals("metro", metros.get(0).isMetro);
	   
	}
	@Test
	public void affhicherClassID() throws IOException {
	
		Metro m=new Metro();
	    Collections.sort(metros, new IdCompara()); 
	    System.out.println(m.Afficher(metros));
	    
	}
	public void affhicher() throws IOException {
		
		Metro m=new Metro();
	    System.out.println(m.Afficher(metros));
	    
	}
	@Test
	public void affhicherMetro() throws IOException {
	

	    Collections.sort(metros, new IdCompara()); 
	    System.out.println(Metro.AfficherMetro(metros));
	    
	}
	@Test
	public void affhicherTram() throws IOException {
	

	    Collections.sort(metros, new IdCompara()); 
	    System.out.println(Metro.AfficherTram(metros));
	    
	}
	@Test
	public void affhicherRer() throws IOException {
	

	    Collections.sort(metros, new IdCompara()); 
	    System.out.println(Metro.AfficherRer(metros));
	    
	}
	@Test
	public void affhicherBUS() throws IOException {
	
	    Collections.sort(metros, new IdCompara()); 
	    System.out.println(Metro.AfficherBUS(metros));
	    
	}
	@Test
	public void trierMetrotest() throws IOException {
		
		String ss="Bourse";
	    System.out.println(Metro.trierMetro(metros,ss));
	}
	@Test
	public void trierBustest() throws IOException {
		Metro m=new Metro();
		String ss="VOLTAIRE";
	    System.out.println(Metro.trierBus(metros,ss));
	  
	}
	@Test
	public void trierRertest() throws IOException {
		Metro m=new Metro();
		String ss="Boissy-Saint-Léger";
	    System.out.println(Metro.trierRer(metros,ss));
	    
	}
	@Test
	public void zmenu()  throws Exception{
	
	
		int flag=0;
		while(flag==0){

			Scanner myObj = new Scanner(System.in);
			System.out.println("***************");  
			System.out.println(" 0. lire de ficher ");            
			System.out.println(" 1. afficher ");      
			System.out.println(" 2. trir ");      
			System.out.println(" 3. classer par ID");  
			System.out.println(" 9. quit ");  
			System.out.println("***************");    
			String choisir = myObj.nextLine();
			

			switch(choisir) {
				  case "0":
						System.out.println(" 1. lire de ficher par default");
						System.out.println("ratp_arret.txt"); 
						testlire();
						
				break; 
				  case "1":
						System.out.println(" 1. affhicher BUS"); 
						System.out.println(" 2. affhicher Metro");   
						System.out.println(" 3. affhicher Rer"); 
						System.out.println(" 4. affhicher Tram"); 
						String choiBMR = myObj.nextLine();
						switch(choiBMR) {
						  case "1":
							  System.out.println("***************"); 
							  affhicherBUS();
							  System.out.println("***************"); 
						break; 
						  case "2":
							  System.out.println("***************"); 
							  affhicherMetro();
							  System.out.println("***************"); 
						break; 
						  case "3":
							  System.out.println("***************"); 
							  affhicherRer();
							  System.out.println("***************"); 
								break; 
						  case "4":
							  System.out.println("***************"); 
							  affhicherTram();
							  System.out.println("***************"); 
								break; 
						 
						 default :
							 System.out.println("Aucune option");
						break;
					
					}
				break; 
				case "2":
					System.out.println("***************"); 
						System.out.println(" 1. Trier BUS");
						System.out.println(" 2. Trier METRO"); 
						System.out.println(" 3. Trier RER"); 
						System.out.println(" 4. Trier Tram"); 
						System.out.println("***************"); 
						String triBMR = myObj.nextLine();
						switch(triBMR) {
						  case "1":
							  System.out.println("**** Nom de BUS*******"); 
							  String nomStation  = myObj.nextLine();
							  System.out.println(Metro.trierBus(metros,nomStation));
						break; 
						  case "2":
							  System.out.println("**** Nom de Metro*******"); 
							  String nomStation1  = myObj.nextLine();
							  System.out.println(Metro.trierMetro(metros,nomStation1));
						break; 
						  case "3":
							  System.out.println("**** Nom de RER*******"); 
							  String nomStation2  = myObj.nextLine();
							  System.out.println(Metro.trierRer(metros,nomStation2));
								break; 
						  case "4":
							  System.out.println("**** Nom de Tram*******"); 
							  String nomStation3  = myObj.nextLine();
							  System.out.println(Metro.trierTram(metros,nomStation3));
								break; 
						 default :
							 System.out.println("Aucune option");
						break;
					
					}
						
						break; 
				case "3":
					System.out.println("***************"); 
				    
					affhicherClassID();
						
				break; 
				case "9":
					System.exit(1);
				 
					
					break; 
				 default :
					 System.out.println("Aucune option");
				break;
			
			}
		
		}
	}


}