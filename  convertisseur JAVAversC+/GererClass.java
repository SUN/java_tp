
import java.lang.reflect.*;

public class GererClass {
	public static void GererClasstoCPP(String nomClasse, String nomC){
		
		try{
				
		//	Class c = Class.forName(nomClasse);
			Class maClasse=Class.forName(nomClasse);
			System.out.println(maClasse.getName());
			Field fields[] = maClasse.getDeclaredFields();      	
			//Renvoyer un tableau des attributs publics
			
			Method methods[] = maClasse.getDeclaredMethods();    	
			//Renvoyer un tableau des méthodes publiques de la classe incluant celles héritées
			
			Constructor constructeurs[] = maClasse.getDeclaredConstructors();
			//Renvoyer les constructeurs publics de la classe
			
			Class classes[] =maClasse.getClasses();             	 
			//Renvoyer les classes et interfaces publiques qui sont membres de la classe
			
			GererCpp.creerCppfiche(fields,methods,constructeurs,nomClasse,nomC);
		}catch (ClassNotFoundException e){
			System.out.println("error \n ");
			System.out.println(" retapez nom de la class \n ");
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	
	
	}
}
