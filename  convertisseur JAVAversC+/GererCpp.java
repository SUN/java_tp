
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.*;
import java.util.Arrays;
public class GererCpp {

	
	public static void creerCppfiche(Field field[], Method method[], Constructor constructor[], String nom,String nomCPP) throws Exception{
		
		String Header = nomCPP + ".hpp";
		String cppCPP    = nomCPP + ".cpp";
		
		creerHead(Header,field, method,constructor, nomCPP);
		creerCPP(cppCPP,Header,method,constructor,nomCPP);
		
	}
  
	private static void creerCPP(String cppCPP,String cppHeader, Method[] method, Constructor[] constructor, String nom) {
		// TODO Auto-generated method stub
		
		
		try {
			PrintWriter writer = new PrintWriter(cppCPP, "UTF-8");
			writer.println("#include <cstdlib>");
			writer.println("#include <sstream>");
			writer.println("#include <iostream>");
            writer.println("#include <string>");
			writer.println("#include"+" \""+nom+".hpp"+"\"");
            writer.println("using namespace std;");
		for(Constructor constu:constructor) {
				Parameter p[] =constu.getParameters();
				writer.print("// "+nom+"::"+nom+"(" );
				for(int i=0;i<p.length;i++) {
					if(i!=p.length-1) {
                        if(p[i].getType().getSimpleName().equals("String")){
                           writer.print( "string"+",");
                        }
                        else{
                          writer.print( p[i].getType().getSimpleName()+",");
                        }
						
					}
					else {
                        if(p[i].getType().getSimpleName().equals("String")){
                            writer.print( "string");
                        }
                        else{
                            writer.print( p[i].getType().getSimpleName());
                        }
					}
				}
				writer.print( ")" );
				writer.println("{} " );
			}

            for(Method methode:method) {
                if(methode.getName().equals("main"))
                {
                    writer.print("int main (){}");
                }else{
                
                
                Parameter p[] =methode.getParameters();
                if(methode.getReturnType().getSimpleName().equals("String")){
                    writer.print("string");
                }else{
                    writer.print(methode.getReturnType().getSimpleName());
                }
                
                writer.print("  "+methode.getName()+"(");
                for(int i=0;i<p.length;i++) {
                    if(i!=p.length-1) {
                        if(p[i].getType().getSimpleName().equals("String")){
                            writer.print( "string"+",");
                        }else{
                            writer.print( p[i].getType().getSimpleName()+",");
                        }
                        
                    }
                    else {
                        if(p[i].getType().getSimpleName().equals("String")){
                            writer.print( "string");
                        }else{
                            writer.print( p[i].getType().getSimpleName());
                        }
                        
                    }
                }
                writer.println( ") {}" );
                }}
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void creerHead(String cppHeader, Field[] field, Method[] method, Constructor[] constructor,
			String nom) {
		

		
		try {
			PrintWriter writer = new PrintWriter(cppHeader, "UTF-8");
			writer.println("#ifndef _CLASSE_"+nom+"_HPP_");
			writer.println("#define _CLASSE_"+nom+"_HPP_");
			writer.println("#include <cstdlib>");
			writer.println("#include <sstream>");
             writer.println("#include <string>");
			writer.println("#include <iostream>");
            writer.println("using namespace std;");
			writer.println("class "+nom +"{");
	
			 writer.println("private :");
			for(Field fields:field) {
				
				   if(fields.getModifiers()==2) 
				   {
                      
                       if(fields.getType().getSimpleName().equals("String")){
					   writer.println("  "+"string"+"  "+fields.getName()+"; "  );
                       }
                       else{
                           writer.println("  "+fields.getType().getSimpleName()+"  "+fields.getName()+"; "  );
                       }
				   }
				
				}
			
			writer.println("protected :");
			for(Field fields:field) {
				   if(fields.getModifiers()==4) 
				   {
					   
                       if(fields.getType().getSimpleName().equals("String")){
                           writer.println("  "+"string"+"  "+fields.getName()+"; "  );
                       }
                       else{
                           writer.println("  "+fields.getType().getSimpleName()+"  "+fields.getName()+"; "  );
                       }
				   }
				}
			  writer.println("public :");
			for(Field fields:field) {
			 
			   if(fields.getModifiers()==1) 
			   {
				 
                   if(fields.getType().getSimpleName().equals("String")){
                       writer.println("  "+"string"+"  "+fields.getName()+"; "  );
                   }
                   else{
                       writer.println("  "+fields.getType().getSimpleName()+"  "+fields.getName()+"; "  );
                   }
			   }
			
			}
			
			for(Constructor constu:constructor) {
				Parameter p[] =constu.getParameters();
				writer.print("  "+nom+"(" );
				for(int i=0;i<p.length;i++) {
					if(i!=p.length-1) {
                        if(p[i].getType().getSimpleName().equals("String")){
                           writer.print( "string"+",");
                        }else{
                            writer.print( p[i].getType().getSimpleName()+",");
                        }
					}
					else {
                        if(p[i].getType().getSimpleName().equals("String")){
                            writer.print( "string");
                        }else{
                            writer.print( p[i].getType().getSimpleName());
                        }
					}
				}
				writer.print( ")" );
				writer.println("{} " );
			}

			for(Method methode:method) {
                if(methode.getName().equals("main")){
                    writer.println("");
                }else{
                    Parameter p[] =methode.getParameters();
                    if(methode.getReturnType().getSimpleName().equals("String")){
                           writer.print("string");
                    }else{
                        writer.print(methode.getReturnType().getSimpleName());
                    }
                    
                    writer.print("  "+methode.getName()+"(");
                    for(int i=0;i<p.length;i++) {
                        if(i!=p.length-1) {
                            if(p[i].getType().getSimpleName().equals("String")){
                               writer.print( "string"+",");
                            }else{
                                writer.print( p[i].getType().getSimpleName()+",");
                            }
                           
                        }
                        else {
                            if(p[i].getType().getSimpleName().equals("String")){
                                writer.print( "string");
                            }else{
                                writer.print( p[i].getType().getSimpleName());
                            }
                            
                        }
                    }
                    writer.println( ") {}" );
                }
			}
            writer.println("};");
			writer.println("#endif");

			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}


}

//java.lang.Object  livre
