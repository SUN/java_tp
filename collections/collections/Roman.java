import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale; 

public class Roman extends Book{
		private String TypeRoman;
		public Roman(){ 
			super();
			TypeRoman = "aucun";
  		}

		 public Roman(String nom,double prix,String categ, String TypeRoman){
			super(nom, prix, categ);
			this.TypeRoman = TypeRoman;
		  }    
		public String getType() {
			return TypeRoman;
		  } 

		  public void setType(String TypeRoman) {
			this.TypeRoman = TypeRoman;
		  }   

		public String toString(){
				
					StringBuilder sb2 = new StringBuilder(" ");
					sb2.append(super.toString());
					sb2.append(" TypeRoman : ");
					sb2.append(TypeRoman);
					
					return sb2.toString();
		}


				
}
