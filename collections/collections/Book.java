import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale; 
import java.util.Comparator;



public class Book {
				protected String NomBook;
				protected double prixBook;
				protected Date DateAjouter;
				protected SimpleDateFormat formater;
				protected String Categorie;
				protected double Note;
		
				Book() 
				{
		   			NomBook="";
		  		}

				Book(String nom) 
				{
		   			NomBook=nom;
					DateAjouter = new Date();
				   	formater = new SimpleDateFormat("'le' dd/MM/yyyy 'à' hh:mm:ss");
					System.out.println(formater.format(DateAjouter));
		  		}
		
				Book(String nom,double prix) 
				{
		   			NomBook=nom;
					prixBook=prix;
					DateAjouter= new Date();
   				   	formater = new SimpleDateFormat("'le' dd/MM/yyyy 'à' hh:mm:ss");
					
 					//System.out.println(formater.format(DateAjouter));
		  		}
				Book(String nom,double prix,String categ) 
				{
		   			NomBook=nom;
					prixBook=prix;
					DateAjouter= new Date();
   				   	formater = new SimpleDateFormat("'le' dd/MM/yyyy 'à' hh:mm:ss");
 					System.out.println(formater.format(DateAjouter));
					Categorie=categ;
		  		}
			
				public String getBook() 
				{
                    
					return NomBook;
				}
				public void setBook(String nom) 
				{
					NomBook=nom;
				}
				public double getPrix() 
				{
                    
					return prixBook;
				}
				public void setPrix(double prix) 
				{
                    prixBook=prix;
				
				}
				public String toString(){
					StringBuilder sb = new StringBuilder("NomBook: ");
					sb.append(NomBook);
					sb.append("  Prix: ");
					sb.append(prixBook);
					sb.append("  Categorie: ");
					sb.append(Categorie);
					sb.append("  Date Ajouté: ");
					sb.append(formater.format(DateAjouter));
					return sb.toString();
				}

		

				

						
				
}

 class NameCompara implements Comparator <Book>
{		@Override
		public int compare(Book book1, Book book2){
		return (book1.NomBook.compareTo(book2.NomBook));
		}
}


 class PrixCompara implements Comparator <Book>
{		@Override
		public int compare(Book book1, Book book2){
		return (book1.prixBook.compareTo(book2.prixBook));
		}
}
 class DateCompara implements Comparator <Book>
{		@Override
		public int compare(Book book1, Book book2){
		return (book1.DateAjouter.compareTo(book2.DateAjouter));
		}
}

/*
Exemple: 
Collections.sort(produits, new Comparator<Produit>() {
    @Override
    public int compare(Produit p1, Produit p2) {
        if(p1.getQuantite().compareTo(p2.getQuantite()) == 0) { // Si la quantite est égale pour les deux produits comparés
          return p1.getPrix().compareTo(p2.getPrix()); // Alors on compare les prix
        } else { // Sinon, les produits ont une quantite différente
          return p1.getQuantite().compareTo(p2.getQuantite()); // Le tri doit s'effectuer sur la quantite
        }
    }
});



*/


