package tp3;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale; 
import java.util.Comparator;

public class Metro {
				public long idStation;
			
				protected String Coordonnee;
				protected String geographique;
				protected String nomStation;
				protected String arrodisement;
				protected String isMetro;
		
	protected  String Afficher(List<Metro> metros){
			StringBuilder string1 = new StringBuilder();
			for (Metro metro : metros) {
			string1.append(metro.toString());
			string1.append(System.lineSeparator());
			}
			return string1.toString();

		 }
		 public String toString(){
				StringBuilder sb = new StringBuilder("id : ");
				sb.append(idStation);
				sb.append(" Station ");
				sb.append(nomStation);
				sb.append(" ");
				sb.append(isMetro);
				return sb.toString();
			}  
	
		 static  String AfficherMetro(List<Metro> metros){
				StringBuilder string1 = new StringBuilder();
				for (Metro metro : metros) {
					if((metro.isMetro).equals("metro")) {
						string1.append(metro.toString());
						string1.append(System.lineSeparator());
					}
				}
				return string1.toString();

			 }
		 static  String AfficherBUS(List<Metro> metros){
				StringBuilder string1 = new StringBuilder();
				for (Metro metro : metros) {
					if((metro.isMetro).equals("bus")) {
						string1.append(metro.toString());
						string1.append(System.lineSeparator());
					}
				}
				return string1.toString();

			 }
		 static  String AfficherTram(List<Metro> metros){
				StringBuilder string1 = new StringBuilder();
				for (Metro metro : metros) {
					if((metro.isMetro).equals("tram")) {
						string1.append(metro.toString());
						string1.append(System.lineSeparator());
					}
				}
				return string1.toString();

			 }
		 static  String AfficherRer(List<Metro> metros){
				StringBuilder string1 = new StringBuilder();
				for (Metro metro : metros) {
					if((metro.isMetro).equals("rer")) {
						string1.append(metro.toString());
						string1.append(System.lineSeparator());
					}
				}
				return string1.toString();

			 }
		 
		   static String trierMetro(List<Metro> metros, String s){
				StringBuilder string1 = new StringBuilder();
				for (Metro metro : metros) {
					if((metro.isMetro).equals("metro")&&(metro.nomStation).equals(s)) {
						string1.append(metro.toString());
						string1.append(System.lineSeparator());
					}
				}
				return string1.toString();

			 }
		   static String trierBus(List<Metro> metros, String s){
				StringBuilder string1 = new StringBuilder();
				for (Metro metro : metros) {
					if((metro.isMetro).equals("bus")&&(metro.nomStation).equals(s)) {
						string1.append(metro.toString());
						string1.append(System.lineSeparator());
					}
				}
				return string1.toString();

			 }
		   static String trierTram(List<Metro> metros, String s){
				StringBuilder string1 = new StringBuilder();
				for (Metro metro : metros) {
					if((metro.isMetro).equals("tram")&&(metro.nomStation).equals(s)) {
						string1.append(metro.toString());
						string1.append(System.lineSeparator());
					}
				}
				return string1.toString();

			 }
		   static String trierRer(List<Metro> metros, String s){
				StringBuilder string1 = new StringBuilder();
				for (Metro metro : metros) {
					if((metro.isMetro).equals("rer")&&(metro.nomStation).equals(s)) {
						string1.append(metro.toString());
						string1.append(System.lineSeparator());
					}
				}
				return string1.toString();

			 }
		
		 public class IdCompara implements Comparator <Metro>{
				
				@Override
				public int compare(Metro m1, Metro m2){
					return Long.compare(m1.idStation, m2.idStation);
				}
							
			}
		
}


