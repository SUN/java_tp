package tp3;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale; 
import java.util.Comparator;
public class IdCompara implements Comparator <Metro>{
			
	@Override
	public int compare(Metro m1, Metro m2){
		return Long.compare(m1.idStation, m2.idStation);
	}
				
}
