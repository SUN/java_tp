package tp4;

import java.util.List;

public class City{

		protected double lontitude;
		protected double latitude;
		protected int weatherId;
		protected String weathermain;
		protected String weatherdescrip;
		protected String  weathericon;
		protected String  weatherbase;
		protected double mainTemp;
		protected int pressure;
		protected int humidity;
		protected double temp_min;
		protected double temp_max;
		protected int visibility;
		protected int windSpeed;
		protected int cloud;
		protected long dtCloud;
		protected int sysType;
		protected int sysId;
		protected String sysName;
		protected long sysSunrise;
		protected long sysSunset;
		protected int sysTimeZone;
		protected long sysTimezoneID;
		protected String sysCityname;
		protected int sysCod;
	    
		private List<City> citys;

		public void setLontitude(double lont) {lontitude=lont;}
		public void setLatitude(double lati) {latitude=lati;}
		public void setWeatherId(int weathid) {weatherId=weathid;}
		public void setWeathermain(String weatmain) {weathermain=weatmain;}
		public void setWeatherdescrip(String weatdesp) {weatherdescrip=weatdesp;}
		public void setWeathericon(String weaticon) {weathericon=weaticon;}
		public void setWeatherbase(String weatbase) {weatherbase=weatbase;}
		public void setmainTemp(double maintemp) {mainTemp=maintemp;}
		public void setpressure(int press) {pressure=press;}
		public void sethumidity(int humi) {humidity=humi;}
		public void settempMin(double tempMin) {temp_min=tempMin;}
		public void settempMan(double tempMan) {temp_max=tempMan;}
		public void setvisibility(int visi) {visibility=visi;}
		public void setwindSpeed(int speed) {windSpeed=speed;}
		public void setcloud(int clo) {cloud=clo;}
		public void setdtCloud(long clodt) {dtCloud=clodt;}
		public void setsysType(int systype) {sysType=systype;}
		public void setsysID(int sysid) {sysId=sysid;}
		public void setsysName(String sysname) {sysName=sysname;}
		public void setsysSunrise(long sunraise) {sysSunrise=sunraise;}
		public void setsysSunset(long sunset) {sysSunset=sunset;}
		public void setsysTmeZone(int timezone) {sysTimeZone=timezone;}
		public void setsysTmeZoneID(long timezoneid) {sysTimezoneID=timezoneid;}
		public void setsysZoneName(String zonename) {sysCityname=zonename;}
		public void setsysZoneCod(int syscod) {sysCod=syscod;}
		
}
