package tp4;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.concurrent.Phaser;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import com.google.gson.*;

public class Main {
	
	
	public void networkConnecter() {
		HttpURLConnection urlConnection =null;
		try {
		URL url = new URL("https://api.openweathermap.org/data/2.5/weather?q=clermont-ferrand&appid=c62ff9852f25d0252f6681f2d0205609");
		urlConnection = (HttpURLConnection) url.openConnection();
		
		InputStream  in = new  BufferedInputStream(urlConnection.getInputStream ());
		readStream(in);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		
			if(urlConnection !=null) {
				urlConnection.disconnect();
			}
		}
	}
	
	public void readStream(InputStream input) {
		try{
			InputStreamReader  readContenu=new  InputStreamReader(input);
			BufferedReader  in = new  BufferedReader(readContenu);
			String  texte = in.readLine ();
			jsonPasrer(texte);
			System.out.println(texte);
		} catch( Exception e){
			System.out.println("error");
			e.printStackTrace ();
		}

	}

	public void jsonPasrer(String text) throws JSONException {
	

		 	JSONObject jsonObject = new JSONObject(text);
		//    JSONArray weatherArray = jsonObject.getJSONArray("weather");
		  //  JSONObject weatherObject = (JSONObject)weatherArray.get(0);
		 	/*	JSONArray weatherArray = weatherObject.getJSONArray("");
		 	JSONObject wrObject = (JSONObject)weatherArray.get(0);
		 	System.out.println("id value: " + wrObject.get("id"));*/
		 	
		 	JSONObject coordObject = jsonObject.getJSONObject("coord");
		 	System.out.println("lon value: " + coordObject.get("lon"));
			System.out.println("lat value: " + coordObject.get("lat"));
		 	
		    JSONObject mainObject = jsonObject.getJSONObject("main");
		    System.out.println("pressure value: " + mainObject.get("pressure"));
		    System.out.println("temp value: " + mainObject.get("temp"));
		    System.out.println("humidity value: " + mainObject.get("humidity"));
		    System.out.println("temp_min value: " + mainObject.get("temp_min"));
		    System.out.println("temp_max value: " + mainObject.get("temp_max"));

		    JSONArray weatherArray = jsonObject.getJSONArray("weather");
		    JSONObject weatherObject = (JSONObject)weatherArray.get(0);
		    System.out.println("id value: " + weatherObject.get("id"));
		    System.out.println("main value: " + weatherObject.get("main"));
		    System.out.println("description value: " + weatherObject.get("description"));
		    System.out.println("icon value: " + weatherObject.get("icon"));
		
		
		 	
		    
	}

	public void networkConnecter(String namecity,String keyAPI) {
		HttpURLConnection urlConnection =null;
		try {
	    URL myURL = new URL("https://api.openweathermap.org/data/2.5/weather?q="+namecity+"&appid="+keyAPI);
			
		urlConnection = (HttpURLConnection) myURL.openConnection();
		InputStream  in = new  BufferedInputStream(urlConnection.getInputStream ());
		readStream(in);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		
			if(urlConnection !=null) {
				urlConnection.disconnect();
			}
		}
	}
	

	public static void main (String[] args)  {
		Main main =new Main();
		main.networkConnecter("clermont-ferrand","c62ff9852f25d0252f6681f2d0205609");

	}		
}