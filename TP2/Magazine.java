import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale; 

public class Magazine extends Book{
		private String Periode;
		public Magazine(){ 
			super();
			Periode = "aucun";
  		}

		 public Magazine(String nom,double prix,String categ, String Periode){
			super(nom, prix, categ);
			this.Periode = Periode;
		  }    
		public String getPeriode() {
			return Periode;
		  } 

		  public void setPeriode(String Periode) {
			this.Periode = Periode;
		  }   

		public String toString(){
				
					
					StringBuilder sb1 = new StringBuilder(" ");
					sb1.append(super.toString());
					sb1.append(" Periode : ");
					sb1.append(Periode);
					
					return sb1.toString();
		}


				
}
