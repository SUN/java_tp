import java.io.*;
import java.util.Scanner;
import java.util.List;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
public class Main {

		 private List<Book> books;
		 static int flag=0;
		 protected void ajouter(Book book){books.add(book);}

        
		 protected  void ajouter(String n,double p,String g){
		 	Book book=new Book(n,p,g);
			books.add(book);
		 }
 		protected  void ajouterM(String n,double p,String g,String c){

	
			Book magazine=new Magazine(n,p,g,c);
			books.add(magazine);
		 	
		 }
		protected  void ajouterR(String n,double p,String g,String c){

	
			Book roman=new Roman(n,p,g,c);
			books.add(roman);
		 	
		 }

         protected  String Afficher(){
			StringBuilder string1 = new StringBuilder();
			for (Book book : books) {
				string1.append(book.toString());
				string1.append(System.lineSeparator());
				}
			return string1.toString();

		 }
   	
		 protected  String Afficher(String s){
			StringBuilder string1 = new StringBuilder();
			switch(s) {
			  case "1":
				for (Book book : books) {
				if(book instanceof Roman )
					string1.append(book.toString());
					string1.append(System.lineSeparator());
				}
				break;
			  case "2":
				for (Book book : books) {
				if(book instanceof Magazine)
					string1.append(book.toString());
					string1.append(System.lineSeparator());
				}
				break;
			  default:
			
				for (Book book : books) {
					string1.append(book.toString());
					string1.append(System.lineSeparator());
				}
					
			}

			return string1.toString();

		 }
 		protected  String AfficherParPrix(String s){
			StringBuilder string2 = new StringBuilder();
			
			return string2.toString();

		 }
		
		 public void Menu(){
			books = new ArrayList<Book>();

			while(flag==0){

				Scanner myObj = new Scanner(System.in);
				System.out.println("***************");  
				System.out.println(" 0. ajouter des books par defaut ");            
				System.out.println(" 1. ajouter ");      
				System.out.println(" 2. afficher ");      
				System.out.println(" 3. trier ");  
				System.out.println(" 4. classer ");  
				System.out.println(" 9. quit ");  
				System.out.println("***************");    
				String choisir = myObj.nextLine();
				
			  
				switch(choisir) {
  				  case "0":
						ajouter("hsd",10.25,"Autre");
						ajouter("sfg",11.48,"Autre");
						ajouter("ztf",12.12,"Autre");
						ajouter("zef",48.65,"Autre");
						ajouter("nvb",78.23,"Autre");
						ajouterM("dfg",98.56,"Autre","Year");
						ajouterM("oip",12.32,"Autre","Year");
						ajouterM("rte",25.01,"Autre","Year");
						ajouterM("zef",66.31,"Autre","Year");
						ajouterM("sds",20.45,"Autre","Year");
						ajouterR("ezr",55.12,"Autre","Autre");
						ajouterR("zce",15.63,"Autre","Autre");
						ajouterR("qsq",28.96,"Autre","Autre");
						ajouterR("ytu",74.36,"Autre","Autre");
						ajouterR("pml",85.36,"Autre","Autre");
					break;
				  case "1":
						System.out.println("***************");   
						Scanner myBook= new Scanner(System.in);       
						System.out.println(" Nom de book ? ");  
						String NomBook = myObj.nextLine();
						System.out.println("Enregistrer book : <<" + NomBook+">>");
						System.out.println(" prix de book ? ");  
						String PrixBook = myObj.nextLine();
						double Prix = Double.parseDouble(PrixBook);
						System.out.println("Enregistrer prix book :  $" + Prix+" ");
						System.out.println(" Categorie de book ?     ( Roman /  Magazine/  Autre)");  
						String CateBook = myObj.nextLine();
					
						switch(CateBook) {
							 case "Roman":
								System.out.println("Roman"); 
								System.out.println(" Type Roman  ? ( Romantique /  Horreur /  Autre)");  
								String TypeRoman = myObj.nextLine();
								System.out.println("Enregistrer Periode Magazine :  " + TypeRoman+" ");
								ajouterR(NomBook,Prix,CateBook,TypeRoman);
								System.out.println("***************"); 
							
							  break;
							 case "Magazine":
								System.out.println(" Periode Magazine  ? ( Year /  Month/  Week)");  
								String PeriodeMaga = myObj.nextLine();
								System.out.println("Enregistrer Periode Magazine :  " + PeriodeMaga+" ");
								ajouterM(NomBook,Prix,CateBook,PeriodeMaga);
								System.out.println("***************"); 
								
								
							  break;
							 default:
								System.out.println(" Autre  ");  
								ajouter(NomBook,Prix,CateBook);
			
						}
			
						
						System.out.println("***************");   
					
					break;
				  case "2":

						System.out.println("***************"); 
					
						System.out.println(Afficher());
					
						System.out.println("***************");    
						
					break;
	 			  case "3": 
						System.out.println(" 1. Trier Roman"); 
						System.out.println(" 2. Trier Magazine"); 
						System.out.println(" 3. Trier All"); 
						String Trier = myObj.nextLine();
						System.out.println("***************"); 
						System.out.println(Afficher(Trier));


					break;
				  case "4": 
						System.out.println(" 1.ordre alpha ́etique"); 
						System.out.println(" 2. par date de sortie"); 
					//	System.out.println(" 3. par le prix");
						String ClasserBook = myObj.nextLine();
						
						//Collections.sort(books, new PrixCompara()); 
						//Collections.sort(books, new NameCompara()); 

						switch(ClasserBook) {
							 case "1":
								Collections.sort(books, new NameCompara()); 

							
							  break;
							 case "2":
								
								Collections.sort(books, new DateCompara()); 
								
							  break;
							case "3":
								
								Collections.sort(books, new PrixCompara());   // probleme
								
							  break;
							 default:
								
			
						}
						
						
						System.out.println(Afficher());

					break;
	 			  case "9":
						System.exit(1);
					break;

				  default:
						System.out.println(" reChoisir "); 
				}
		}
}
	   	 
	public static void main (String[] args) {
				
			Main main = new Main();
			main.Menu();
			
		 	
		
	}


		


}
