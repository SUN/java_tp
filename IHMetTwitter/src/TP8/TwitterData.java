package TP8;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import twitter4j.MediaEntity;
import twitter4j.Status;
import twitter4j.User;

public class TwitterData {
	private MediaEntity[] mediaEntity;
    private String name;
    private String text;
    private URL url;
    private ImageIcon usericon;
    private  ImageIcon[] image;
	public TwitterData(Status statu) throws IOException {
		User user = statu.getUser();
		URL url = new URL(user.getMiniProfileImageURL());
    	BufferedImage userImage = ImageIO.read(url);
    	usericon = new ImageIcon(userImage);

    	mediaEntity=statu.getMediaEntities();
    	image = new ImageIcon[mediaEntity.length];
    	for(int i = 0; i < mediaEntity.length; i++) {
    		URL urlim = new URL(mediaEntity[i].getMediaURL());
    		BufferedImage buffim = ImageIO.read(urlim);
    		image[i] = new ImageIcon(buffim);
    	}
		name=statu.getUser().getName() ;
		text= statu.getText();
		

	}
	public MediaEntity[] getMediaEntity() {
		return mediaEntity;
	}
	public void setMediaEntity(MediaEntity[] mediaEntity) {
		this.mediaEntity = mediaEntity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public URL getUrl() {
		return url;
	}
	public void setUrl(URL url) {
		this.url = url;
	}
	public ImageIcon getUsericon() {
		return usericon;
	}
	public void setUsericon(ImageIcon usericon) {
		this.usericon = usericon;
	}
	public ImageIcon[] getImage() {
		return image;
	}
	public void setImage(ImageIcon[] image) {
		this.image = image;
	}
	@Override
	public String toString() {
		
		return "  name=" + name +  " public =" + text;
		
	
	}
	

}
