package TP8;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.ImageObserver;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.awt.HeadlessException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.apple.eawt.Application;

import twitter4j.MediaEntity;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
 
public class Mvc1  extends JFrame implements ActionListener {
	 private static JButton button2;
	 private static JButton button1;
	 private static JButton button0;
	 private static JLabel label1 ;
	 private static TwitterData statu;
	 private static JLabel label ;
	 private static JTextArea textUsenane;
	 private static  JTextArea textShow2;
	 private static JTextField text;
	 private static JScrollPane jspane;
	 private static JScrollPane jspaneshow2;
	 public static int numberNuser;
	 public static List<Status> tweetsTwitter;
	 private static  JFrame f;
	 private  ImageIcon icon;
	 private  ImageIcon iconUser;
	 private  ImageIcon[] iconimage;
	 private static JLabel[] labelimage ;


	  static JPanel card;
   	 static CardLayout cl;
	public Mvc1() {
		     f = new JFrame("Twitter");
	        card = new JPanel();
	        label = new JLabel();
	        icon=new ImageIcon("image/twitter.png");
	        icon.setImage(icon.getImage().getScaledInstance(35,35,Image.SCALE_DEFAULT));
	        button2=new JButton("rearch ");
	        button1=new JButton("next ");
	        button0=new JButton("last");
	        textShow2 = new JTextArea(5,40);
	        text=new JTextField(20);
	        card.add(text);
	        card.add(label);
	        card.add(button2);
	        label.setIcon(icon);
	        button2.addActionListener(this);
	        button1.addActionListener(this);
	        button0.addActionListener(this);
	        text.addActionListener(this);

	}
	

    public static void main(String[] args) {
    	Mvc1 m=new Mvc1();
    	 f.add(card, BorderLayout.CENTER);
    	 f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	 f.setSize(600,110);
    	 f.setLocationRelativeTo(null);
    	 f.setVisible(true);
    }


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==button2){
			card.removeAll();
	    	tweetsTwitter=null;
	    	textUsenane=null;
	    	numberNuser=0;
	    	label1=null;

	    	textShow2.setText("");
	    	String textFieldValue = text.getText();
				ApiCall main=new ApiCall();
            	Twitter twitter = null;
				try {
					twitter = main.ApiCall(textFieldValue);
				} catch (TwitterException e1) {
					e1.printStackTrace();
				}
            	try {
					tweetsTwitter=main.getInformation(textFieldValue, twitter );
				} catch (TwitterException e1) {
					e1.printStackTrace();
				}
            	try {
            		statu=new TwitterData(tweetsTwitter.get(numberNuser));
            	
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            	
            	
             textUsenane= new JTextArea(1,10);
             label1 = new JLabel();
             iconUser=statu.getUsericon();
         
             iconUser.setImage(iconUser.getImage().getScaledInstance(50,50,Image.SCALE_DEFAULT));
             label1.setIcon(iconUser);
             textUsenane.append(statu.getName());
             textShow2.append(statu.getText());
             card.add(text);
 	         card.add(label);
 	         card.add(button2);
             card.add(button1);
             card.add(button0);
             card.add(button0);
             card.add(label1);
             card.add(new JScrollPane(textUsenane));
             card.add(new JScrollPane(textShow2));
         
           labelimage=new JLabel[5];
          	if(statu.getImage()!=null) {
        		iconimage=statu.getImage();
        		for(int i=0;i<iconimage.length;i++) {
        			labelimage[i]=new JLabel(); 
        			iconimage[i].setImage(iconimage[i].getImage().getScaledInstance(100,100,Image.SCALE_DEFAULT));
        			labelimage[i].setIcon(iconimage[i]);
        			card.add( labelimage[i]);
        			  
        		}
        		
        	}
             
             f.setSize(600,600);
           	 f.setLocationRelativeTo(null);
           	 f.setVisible(true);
           
            
		}
		else if(e.getSource()==button1){
			card.removeAll();
	    	tweetsTwitter=null;
	    	textUsenane=null;
	    	 jspaneshow2=null;
	    	label1=null;
	    	jspane=null;
	 
	     	textShow2.setText("");
	    	String textFieldValue = text.getText();
				ApiCall main=new ApiCall();
            	Twitter twitter = null;
				try {
					twitter = main.ApiCall(textFieldValue);
				} catch (TwitterException e1) {
					e1.printStackTrace();
				}
            	try {
					tweetsTwitter=main.getInformation(textFieldValue, twitter );
				} catch (TwitterException e1) {
					e1.printStackTrace();
				}
            	try {
            		numberNuser++;
            		if(numberNuser==tweetsTwitter.size()) {
            			numberNuser=0;
            		}
            		statu=new TwitterData(tweetsTwitter.get(numberNuser));
            		
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            
             textUsenane= new JTextArea(1,10);
             label1 = new JLabel();
             iconUser=statu.getUsericon();
             jspane=new JScrollPane(textUsenane);
             jspaneshow2 =new JScrollPane(textShow2);
             iconUser.setImage(iconUser.getImage().getScaledInstance(50,50,Image.SCALE_DEFAULT));
             label1.setIcon(iconUser);
             textUsenane.append(statu.getName());
             textShow2.append(statu.getText());
             
             card.add(text);
 	         card.add(label);
 	         card.add(button2);
             card.add(button1);
             card.add(button0);
             card.add(button0);
             card.add(label1);
             card.add(jspane);
             card.add(jspaneshow2);
             int longue=iconimage.length;
             labelimage=new JLabel[5];
           	if(statu.getImage()!=null) {
         		iconimage=statu.getImage();
         		for(int i=0;i<iconimage.length;i++) {
         			labelimage[i]=new JLabel(); 
         			iconimage[i].setImage(iconimage[i].getImage().getScaledInstance(100,100,Image.SCALE_DEFAULT));
         			labelimage[i].setIcon(iconimage[i]);
         		
         			  card.add( labelimage[i]);
         			  
         		}
         		
         	}
             f.setSize(600,600);
           	 f.setLocationRelativeTo(null);
           	 f.setVisible(true);
           
		}
		else if(e.getSource()==button0){
			card.removeAll();
	    	tweetsTwitter=null;
	    	textUsenane=null;
	    	 jspaneshow2=null;
	    	label1=null;
	    	jspane=null;
	   
	     	textShow2.setText("");
	    	String textFieldValue = text.getText();
				ApiCall main=new ApiCall();
            	Twitter twitter = null;
				try {
					twitter = main.ApiCall(textFieldValue);
				} catch (TwitterException e1) {
					e1.printStackTrace();
				}
            	try {
					tweetsTwitter=main.getInformation(textFieldValue, twitter );
				} catch (TwitterException e1) {
					e1.printStackTrace();
				}
            	try {
            		numberNuser--;
            		if(numberNuser<0||numberNuser==0) {
            			numberNuser=0;
            		}
            		
            		System.out.println(numberNuser);
            		statu=new TwitterData(tweetsTwitter.get(numberNuser));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            	
          
             textUsenane= new JTextArea(1,10);
             label1 = new JLabel();
             iconUser=statu.getUsericon();
             jspane=new JScrollPane(textUsenane);
             jspaneshow2 =new JScrollPane(textShow2);
             iconUser.setImage(iconUser.getImage().getScaledInstance(50,50,Image.SCALE_DEFAULT));
             label1.setIcon(iconUser);
             textUsenane.append(statu.getName());
             textShow2.append(statu.getText());
             
             card.add(text);
 	         card.add(label);
 	         card.add(button2);
             card.add(button1);
             card.add(button0);
             card.add(button0);
             card.add(label1);
             card.add(jspane);
             card.add(jspaneshow2);   
             int longue=iconimage.length;
             labelimage=new JLabel[5];
           	if(statu.getImage()!=null) {
         		iconimage=statu.getImage();
         		for(int i=0;i<iconimage.length;i++) {
         			labelimage[i]=new JLabel(); 
         			iconimage[i].setImage(iconimage[i].getImage().getScaledInstance(100,100,Image.SCALE_DEFAULT));
         			labelimage[i].setIcon(iconimage[i]);
         		
         			  card.add( labelimage[i]);
         		}
         		
         	}
             f.setSize(600,600);
           	 f.setLocationRelativeTo(null);
           	 f.setVisible(true);
           
		}else{
			
		}
		
	}


}